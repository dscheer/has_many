class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :specialist_id
      t.integer :patient_id
      t.string :complaint
      t.date :appointment_date
      t.integer :fee

      t.timestamps
    end
  end
end
