# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



Patient.create(name: 'James Robinson', street_address: '2614 Chardonnay Drive', insurance_id: 2)
Patient.create(name: 'Bob Sanchez', street_address: '17861 Treelake Ln.', insurance_id: 2)
Patient.create(name: 'Ann Justus', street_address: '4337 Golden Ridge Road', insurance_id: 1)
Patient.create(name: 'Joe Granger', street_address: '17861 Treelake Ln.', insurance_id: 4)
Patient.create(name: 'Helen Gray', street_address: '1604 Williams Avenue', insurance_id: 5)


Specialist.create(name: 'James Tuttle', specialty: 'General Practitioner')
Specialist.create(name: 'Inez Williams', specialty: 'Radiology')
Specialist.create(name: 'Gina  Smith', specialty: 'Osteopathy')
Specialist.create(name: 'Annie Rodriguez', specialty: 'Dermatology')
Specialist.create(name: 'Fred Clark', specialty: 'Oncology')

Insurance.create(name: 'Geico', street_address: '4094 Tea Berry Lane')
Insurance.create(name: 'Blue Cross', street_address: '2855 Pride Avenue')
Insurance.create(name: 'Wal-Mart', street_address: '1053 Emily Drive')
Insurance.create(name: 'Google', street_address: '1376 Memory Lane')
Insurance.create(name: 'Allstate', street_address: '2490 Oakmound Drive')

Appointment.create(specialist_id: 1, patient_id: 1, complaint: "Sore Neck", appointment_date: DateTime.new(2014,10,3), fee: 40)
Appointment.create(specialist_id: 2, patient_id: 1, complaint: "CT Scan", appointment_date: DateTime.new(2014,10,4), fee: 1000)
Appointment.create(specialist_id: 3, patient_id: 2, complaint: "Broken Bone", appointment_date: DateTime.new(2014,10,3), fee: 400)
Appointment.create(specialist_id: 4, patient_id: 2, complaint: "Rash on Back", appointment_date: DateTime.new(2014,10,8), fee: 80)
Appointment.create(specialist_id: 5, patient_id: 3, complaint: "Lung Cancer", appointment_date: DateTime.new(2014,10,14), fee: 2000)
Appointment.create(specialist_id: 1, patient_id: 3, complaint: "Itchy Ear", appointment_date: DateTime.new(2014,10,12), fee: 40)
Appointment.create(specialist_id: 2, patient_id: 4, complaint: "CT Scan", appointment_date: DateTime.new(2014,10,6), fee: 1000)
Appointment.create(specialist_id: 3, patient_id: 4, complaint: "Crooked Back", appointment_date: DateTime.new(2014,10,28), fee: 100)
Appointment.create(specialist_id: 1, patient_id: 5, complaint: "Constant Vomiting", appointment_date: DateTime.new(2014,11,22), fee: 40)
Appointment.create(specialist_id: 4, patient_id: 5, complaint: "Loose Skin", appointment_date: DateTime.new(2014,10,19), fee: 200)
Appointment.create(specialist_id: 1, patient_id: 3, complaint: "Ebola", appointment_date: DateTime.new(2014,10,30), fee: 20000)
Appointment.create(specialist_id: 5, patient_id: 4, complaint: "Skin Cancer", appointment_date: DateTime.new(2014,11,2), fee: 500)





#Country.create(name: 'Germany', population: 81831000)

